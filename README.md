These diagrams are draft-quality explorations supporing the WMDE Technical
Wishes team's Visual Editor template dialog sidebar changes.

Contents
===

Data flow diagrams
---
* `context` - Highest-level overview
* `level 1` - TBD

Building
===

Install prerequisites,

  apt install latexmk

Then,

  make
